#ifndef __INC_MAPPY__
#define __INC_MAPPY__



#ifdef __cplusplus
extern "C" {
#endif

#define FMP_05		0	//FMP 0.5
#define FMP_10		1	//FMP 1.0
#define FMP_10RLE	2	//FMP 1.0 RLE (warning! fake RLE)

//error codes
#define MER_NONE 			0	
#define MER_OUTOFMEM 		1
#define MER_MAPLOADERROR 	2
#define MER_NOOPEN 			3
#define MER_NOSCREEN 		4
#define MER_NOACCELERATION 	5
#define MER_CVBFAILED 		6
#define MER_MAPTOONEW 		7

//anim type
#define AN_END 		0xFF	// end of anims
#define AN_NONE 	0		// No anim defined
#define AN_LOOPF 	1		// Loops from start to end, then jumps to start etc
#define AN_LOOPR 	2		// As above, but from end to start
#define AN_ONCE 	3		// Only plays once
#define AN_ONCEH 	4		// Only plays once, but holds end frame
#define AN_PPFF 	5		// Ping Pong start-end-start-end-start etc
#define AN_PPRR 	6		// Ping Pong end-start-end-start-end etc
#define AN_PPRF 	7		// internal use only
#define AN_PPFR 	8		// internal use only
#define AN_ONCES 	9		// internal use only

typedef struct
{
	char name[5]; //4 char +/0
	int size;
	unsigned char *data;
} CHUNK;

typedef struct
{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} COLORSTR;

typedef struct {
	char name[70];
	char info1[70];
	char info2[70];
	char info3[70];	
} ATHSTR;

typedef struct {
	short int mapwidth, mapheight;				//in block count
	short int mapblockwidth, mapblockheight;	//in pixel
	short int mapdepth;							//8bit, (15bits, 16bits), TrueColor (24bits), (32bits)
	short int mapblockstrsize;					//bytes used by block data (collision, etc.. see BLKSTR), 32bytes on FMP 1.0, dynamique on FMP 0.5
	short int mapnumblockstr;					//num of block
	short int mapnumblockgfx;					//num of gfx for block, <= num of block, since blank block could be added (for later gfx import or data only block)
	unsigned char colourKey;	//8bits
	COLORSTR colourKeyHD;		//24bits
	// NEW for Release 6
	int maptype;							//  0 : FMP 0.5
											//	1 : FMP 1.0
											// 	2 : FMP 1.0 RLE
											//	3 : ????
	int mapclickmask;						//block ID used as mask for pixel perfect click
	int mapblockgapx, mapblockgapy; 		//gap between 2 blocks (useful for iso where x = blockwidth/2 and y = blockheight/2)
	int mapblockstaggerx, mapblockstaggery; //isometric offsets
	// for easier editing
	int mapnumanimstr;
	unsigned char hasAlternateGFX;
	
	// removed from original format
	//	int mapaltdepth;						// ? not saved on file and forced to 8bits so what the need ? 

} HDRSTR;

typedef struct {
	short int xmapoffset;	// editor offset, in blocks, from left.
	short int ymapoffset;	// editor offset, in blocks, from right. 
	long int fgcolour;		// fg colour for text, buttons etc.
	long int bgcolour;		// bg colour for text, buttons etc.
	short int swidth;		// width of current screen res
	short int sheight;		// height of current screen res
	short int strtstr;		// first structure in view
	short int strtblk;		// first block graphic in view
	short int curstr;		// current block structure
	short int curanim;		// current anim structure
	short int animspd;		// gap in frames between anims
	short int span;			// control panel height
	short int numbrushes;	// number of brushes to follow
	short int clickmask;
} EDHDSTR;


// Structure for data blocks
typedef struct {				
	long int bgoff, fgoff;				// offsets from start of graphic blocks (BG, FG1)
	long int fgoff2, fgoff3; 			// (FG2, FG3)
	unsigned long int user1, user2;		// user long data
	unsigned short int user3, user4;	// user short data
	unsigned char user5, user6, user7;	// user byte data
	unsigned char tl : 1;				// bits for collision detection
	unsigned char tr : 1;
	unsigned char bl : 1;
	unsigned char br : 1;
	unsigned char trigger : 1;			// bit to trigger an event
	unsigned char unused1 : 1;
	unsigned char unused2 : 1;
	unsigned char unused3 : 1;
} BLKSTR;

// Animation control structure
typedef struct {		
	signed char antype;		// Type of anim, AN_?
	signed char andelay;	// Frames to go before next frame
	signed char ancount;	// Counter, decs each frame, till 0, then resets to andelay (for rendering only)
	signed char anuser;		// User info (unused ?)
	long int ancuroff;		// Points to current offset in list (for rendering only)
	long int anstartoff;	// Points to start of blkstr offsets list, AFTER ref. blkstr offset
	long int anendoff;		// Points to end of blkstr offsets list
} ANISTR;

// animation storage structure (for easier update) and using short, not long (64K block should be enough !)
typedef struct {		
	signed char antype;		// Type of anim, AN_?
	signed char andelay;	// Frames to go before next frame
	signed char anuser;		// User info (unused ?)
	signed char frameCount;	// size of blocks array
	short int refBlock;		// reference block ID
	short int *blocks;		// list of blocks ID (frame)
} ANMSTR;

CHUNK *mappy_getChunk(char *name);
int mappy_setChunk(char *name, int size, unsigned char *data);

HDRSTR *mappy_getHeader();
void mappy_setHeader(HDRSTR *newHeader);

ATHSTR *mappy_getAuthorInfo();
void mappy_setAuthorInfo(ATHSTR *newAuthor);

COLORSTR *mappy_getColors();
void mappy_setColors( COLORSTR *newColors);

BLKSTR *mappy_getBlocks();
void mappy_setBlocks( BLKSTR *newBlocks );

ANMSTR (*mappy_getAnimations())[];

unsigned char *mappy_getBlockGFX();
void mappy_setBlockGFX(unsigned char *newGFX);
long mappy_getBlockGFXSize();


unsigned char *mappy_getAlternateGFX();
long mappy_getAlternateGFXSize();

signed short int *mappy_getLayer(unsigned char id);
void mappy_setLayer(unsigned char id, signed short int *newMap);

void mappy_new( );
int mappy_load (char *);
int mappy_save (char *);
void mappy_close (void);
int mappy_getLastErrorID();

/*
extern short int ** maparraypt;
extern char * mapblockgfxpt;
extern char * mapblockstrpt;
extern ANISTR * mapanimstrpt;
extern ANISTR * mapanimstrendpt;
extern short int ** mapmappt;
extern short int *** mapmaparraypt;
extern int mapblocksinvidmem, mapblocksinsysmem;
extern int mapblockgapx, mapblockgapy;
extern int mapblockstaggerx, mapblockstaggery;


void MapSetPal8 (void);
void MapCorrectColours (void);
int MapRelocate (void);
int MapDecode (unsigned char *);
int MapLoadMAR (char *, int);
int MapDecodeMAR (unsigned char *, int);
int MapGetBlockID (int, int);
int MapGenerateYLookup (void);
int MapChangeLayer (int);
int MapGetXOffset (int, int);
int MapGetYOffset (int, int);
BLKSTR * MapGetBlockInPixels (int, int);
BLKSTR * MapGetBlock (int, int);
void MapSetBlockInPixels (int, int, int);
void MapSetBlock (int, int, int);
void MapRestore (void);
void MapInitAnims (void);
void MapUpdateAnims (void);
*/

#ifdef __cplusplus
}
#endif

#endif //__INC_MAPPY__