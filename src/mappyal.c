/* Mappy playback library functions and variables
 * (C)2001 Robin Burrows - rburrows@bigfoot.com
 * This version (R11C) released May 2002 - for Mappy FMP maps
 * Please read the readmeal.txt file and look at the examples
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "mappy.h"

#ifdef __cplusplus
#error "Hey, stop compiling mappyal.c as C++!, see MappyAL readme.txt"
#endif

// Comment out next line to disable index0 to truecolour pink conversion
#define RB8BITTOPINK

#define MAX_CHUNK	256		//256 should be enough, since NLYR is limited to 100

static int maperror;		// Set to a MER_ error if something wrong happens
		

static FILE * mapfilept;
static bool mapIsLSB;


static CHUNK chunks[MAX_CHUNK]; 


static ATHSTR *author;
static HDRSTR *header;
static EDHDSTR *preferences;
static COLORSTR *palette;
static BLKSTR *blocks;
static ANMSTR (*animations)[];
static unsigned char *alternateGFX;
static unsigned char *blockGFX;
signed short int *layers[8];

char mapnovctext[70];

static char debug_string[255];
static void debug_printf(char *comment)
{
#ifdef DEBUG
	printf("%s",comment);
#endif
}

static int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}

//?? chunk size read isn't according mapIsLSB ??
static int getChunkSize ( unsigned char * locpt)
{
	return ((((unsigned int) (locpt[0]))<<24)|(((unsigned int) (locpt[1]))<<16)|(((unsigned int) (locpt[2]))<<8)|((unsigned int) (locpt[3])));
}
static void setChunkSize ( unsigned char *locpt, int size)
{
	*(locpt+0) = (size >> 24) & 0xFF;
	*(locpt+1) = (size >> 16) & 0xFF;
	*(locpt+2) = (size >>  8) & 0xFF;
	*(locpt+3) = (size >>  0) & 0xFF;
}

static int getShort(unsigned char * locpt)
{
	int rval;

	if (mapIsLSB)
		rval = ((((int) (locpt[1]))<<8)|((int) (locpt[0])));
	else
		rval = ((((int) (locpt[0]))<<8)|((int) (locpt[1])));
	
	if (rval & 0x8000) rval -= 0x10000;
	
	return rval;
}
static void setShort(unsigned char * locpt, short int value)
{
	if (mapIsLSB)
	{
		locpt[0] = (value & 0xFF);
		locpt[1] = (value >> 8 ) & 0xFF;
	}
	else
	{
		locpt[1] = (value & 0xFF);
		locpt[0] = (value >> 8 ) & 0xFF;
	}
}


static int getLong (unsigned char * locpt)
{
	if (mapIsLSB)
		return ((((int) (locpt[3]))<<24)|(((int) (locpt[2]))<<16)|(((int) (locpt[1]))<<8)|((int) (locpt[0])));
	else
		return ((((int) (locpt[0]))<<24)|(((int) (locpt[1]))<<16)|(((int) (locpt[2]))<<8)|((int) (locpt[3])));
}
static void setLong(unsigned char * locpt, long value)
{
	if (mapIsLSB)
	{
		locpt[0] = (value & 0xFF);
		locpt[1] = (value >> 8 ) & 0xFF;
		locpt[2] = (value >> 16 ) & 0xFF;
		locpt[3] = (value >> 24 ) & 0xFF;
	}
	else
	{
		locpt[3] = (value & 0xFF);
		locpt[2] = (value >> 8 ) & 0xFF;
		locpt[1] = (value >> 16 ) & 0xFF;
		locpt[0] = (value >> 24 ) & 0xFF;
	}
}

static int addChunk(char *name, int size, unsigned char *data)
{
	int i;
	
	if (strlen(name) != 4)	
	{
		debug_printf("Chunk name must be 4 chars long\n");
		return -1;
	}
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if (chunks[i].size == 0)
		{
			memcpy(chunks[i].name, name, 4);
			chunks[i].size = size;
			chunks[i].data = data;
			return 0;
		}
	}
	
	debug_printf("mappyCLI only support up to 256 chunks");
	return -1;
}




/*
int MapGenerateYLookup (void)
{
int i, j;

	for (i=0;i<8;i++) {
		if (mapmaparraypt[i]!=NULL) { free (mapmaparraypt[i]); mapmaparraypt[i] = NULL; }
		if (mapmappt[i]!=NULL) {
			mapmaparraypt[i] = malloc (mapheight*sizeof(short int *));
			if (mapmaparraypt[i] == NULL) return -1;
			for (j=0;j<mapheight;j++) mapmaparraypt[i][j] = (mapmappt[i]+(j*mapwidth));
			if (mapmappt[i] == mappt) maparraypt = mapmaparraypt[i];
		}
	}
	return 0;
}

static int MEClickmask (int x, int y, int xory)
{
	if (abmTiles == NULL) return 0;

	x %= mapblockgapx; y %= mapblockgapy;

	if (x >= mapblockwidth && xory == 0) return 0;
	if (x >= mapblockwidth && xory == 1) {
		if (y < mapblockstaggery) return -1;
		else return 1;
	}
	if (y >= mapblockheight && xory == 1) return 1;
	if (y >= mapblockheight && xory == 0) {
		if (x < mapblockstaggerx) return -1;
		else return 0;
	}

	switch (mapdepth) {
		case 8:
			if (getpixel (abmTiles[mapclickmask], x, y) == 0) {
				if (x < (mapblockwidth/2) && xory == 0) return -1;
				if (x >= (mapblockwidth/2) && xory == 0) return 0;
				if (y < (mapblockheight/2) && xory == 1) return -1;
				if (y >= (mapblockheight/2) && xory == 1) return 1;
			}
			return 0;
		default:
			if (getpixel (abmTiles[mapclickmask], x, y) == makecol (255,0,255)) {
				if (x < (mapblockwidth/2) && xory == 0) return -1;
				if (x >= (mapblockwidth/2) && xory == 0) return 0;
				if (y < (mapblockheight/2) && xory == 1) return -1;
				if (y >= (mapblockheight/2) && xory == 1) return 1;
			}
			return 0;
	}
	return 0;
}

int MapGetXOffset (int xpix, int ypix)
{
int xb;

	if (mapblockstaggerx || mapblockstaggery) {
		xpix += (mapblockstaggerx);
		ypix += (mapblockstaggery);
	}
	xb = xpix/mapblockgapx;

	if ((mapblockstaggerx || mapblockstaggery) && mapclickmask) xb += MEClickmask (xpix, ypix, 0);

	if (xb < 0) xb = 0;
	if (xb >= mapwidth) xb = mapwidth-1;
	return xb;
}

int MapGetYOffset (int xpix, int ypix)
{
int yb;

	if (mapblockstaggerx || mapblockstaggery) {
		xpix += (mapblockstaggerx);
		ypix += (mapblockstaggery);
	}
	yb = ypix/mapblockgapy;

	if ((mapblockstaggerx || mapblockstaggery) && mapclickmask) {
		yb *= 2;
		yb += MEClickmask (xpix, ypix, 1);
	}

	if (yb < 0) yb = 0;
	if (yb >= mapheight) yb = mapheight-1;
	return yb;
}

BLKSTR * MapGetBlockInPixels (int x, int y)
{
int xp, yp;
short int * mymappt;
ANISTR * myanpt;

	if (x < 0 || y < 0 || x >= (mapwidth*mapblockwidth) || y >= (mapheight*mapblockheight)) return NULL;

	xp = x; yp = y;
	x = MapGetXOffset (xp, yp);
	y = MapGetYOffset (xp, yp);

	if (maparraypt!= NULL) {
		mymappt = maparraypt[y]+x;
	} else {
		mymappt = mappt;
		mymappt += x;
		mymappt += y*mapwidth;
	}
	if (*mymappt>=0) return ((BLKSTR*) mapblockstrpt) + *mymappt;
	else { myanpt = mapanimstrendpt + *mymappt;
		return ((BLKSTR *) mapblockstrpt) + mapanimseqpt[myanpt->ancuroff]; }
}

BLKSTR * MapGetBlock (int x, int y)
{
short int * mymappt;
ANISTR * myanpt;

	if (maparraypt!= NULL) {
		mymappt = maparraypt[y]+x;
	} else {
		mymappt = mappt;
		mymappt += x;
		mymappt += y*mapwidth;
	}
	if (*mymappt>=0) return ((BLKSTR*) mapblockstrpt) + *mymappt;
	else { myanpt = mapanimstrendpt + *mymappt;
		return ((BLKSTR *) mapblockstrpt) + mapanimseqpt[myanpt->ancuroff]; }
}

void MapSetBlockInPixels (int x, int y, int strvalue)
{
int xp, yp;
short int * mymappt;

	if (x < 0 || y < 0 || x >= (mapwidth*mapblockwidth) || y >= (mapheight*mapblockheight)) return;
	xp = x; yp = y;
	x = MapGetXOffset (xp, yp);
	y = MapGetYOffset (xp, yp);

	if (maparraypt!= NULL) {
		mymappt = maparraypt[y]+x;
	} else {
		mymappt = mappt;
		mymappt += x;
		mymappt += y*mapwidth;
	}
	*mymappt = strvalue;
}

void MapSetBlock (int x, int y, int strvalue)
{
short int * mymappt;

	if (maparraypt!= NULL) {
		mymappt = maparraypt[y]+x;
	} else {
		mymappt = mappt;
		mymappt += x;
		mymappt += y*mapwidth;
	}
	*mymappt = strvalue;
}

int MapChangeLayer (int newlyr)
{
	if (newlyr<0 || newlyr>7 || mapmappt[newlyr] == NULL) return -1;
	mappt = mapmappt[newlyr]; maparraypt = mapmaparraypt[newlyr];
	return newlyr;
}

int MapGetBlockID (int blid, int usernum)
{
int i;
BLKSTR * myblkpt;

	myblkpt = (BLKSTR *) mapblockstrpt;
	if (myblkpt == NULL) return -1;

	for (i=0;i<mapnumblockstr;i++) {
		switch (usernum) {
			case 1:
				if (myblkpt[i].user1 == blid) return i;
				break;
			case 2:
				if (myblkpt[i].user2 == blid) return i;
				break;
			case 3:
				if (myblkpt[i].user3 == blid) return i;
				break;
			case 4:
				if (myblkpt[i].user4 == blid) return i;
				break;
			case 5:
				if (myblkpt[i].user5 == blid) return i;
				break;
			case 6:
				if (myblkpt[i].user6 == blid) return i;
				break;
			case 7:
				if (myblkpt[i].user7 == blid) return i;
				break;
		}
	}

	return -1;
}

int MapDecodeMAR (unsigned char * mrpt, int marlyr)
{
int i, j;
short int * mymarpt;

	if (marlyr < 0 || marlyr > 7) return -1;

	if (mapmappt[marlyr] == NULL)
		mapmappt[marlyr] = malloc (mapwidth*mapheight*sizeof(short int));

	memcpy (mapmappt[marlyr], mrpt, (mapwidth*mapheight*sizeof(short int)));

	mymarpt = mapmappt[marlyr];
	j = 0; for (i=0;i<(mapwidth*mapheight);i++) { if (mymarpt[i]&0xF) j = 1; }
	if (j == 0) {
		for (i=0;i<(mapwidth*mapheight);i++) {
			if (mymarpt[i] >= 0) mymarpt[i] /= 32;
			else mymarpt[i] /= 16;
		}
	}

	return 0;
}

int MapLoadMAR (char * mname, int marlyr)
{
int i, j;
short int * mymarpt;
PACKFILE * marfpt;

	if (marlyr < 0 || marlyr > 7) return -1;

	marfpt = pack_fopen (mname, "rp");
	if (marfpt==NULL) { marfpt = pack_fopen (mname, "r");
		if (marfpt==NULL) { return -1; } }

	if (mapmappt[marlyr] == NULL)
		mapmappt[marlyr] = malloc (mapwidth*mapheight*sizeof(short int));

	if (pack_fread (mapmappt[marlyr], (mapwidth*mapheight*sizeof(short int)), marfpt) !=
		(mapwidth*mapheight*sizeof(short int))) { pack_fclose (marfpt); return -1; }

	pack_fclose (marfpt);

	mymarpt = mapmappt[marlyr];
	j = 0; for (i=0;i<(mapwidth*mapheight);i++) { if (mymarpt[i]&0xF) j = 1; }
	if (j == 0) {
		for (i=0;i<(mapwidth*mapheight);i++) {
			if (mymarpt[i] >= 0) mymarpt[i] /= 32;
			else mymarpt[i] /= 16;
		}
	}

	return 0;
}

void MapInitAnims (void)
{
ANISTR * myanpt;
	if (mapanimstrpt==NULL) return;
	myanpt = (ANISTR *) mapanimstrendpt; myanpt--;
	while (myanpt->antype!=-1)
	{
		if (myanpt->antype==AN_PPFR) myanpt->antype = AN_PPFF;
		if (myanpt->antype==AN_PPRF) myanpt->antype = AN_PPRR;
		if (myanpt->antype==AN_ONCES) myanpt->antype = AN_ONCE;
		if ((myanpt->antype==AN_LOOPR) || (myanpt->antype==AN_PPRR))
		{
		myanpt->ancuroff = myanpt->anstartoff;
		if ((myanpt->anstartoff)!=(myanpt->anendoff)) myanpt->ancuroff=(myanpt->anendoff)-1;
		} else {
		myanpt->ancuroff = myanpt->anstartoff;
		}
		myanpt->ancount = myanpt->andelay;
		myanpt--;
	}
}

void MapUpdateAnims (void)
{
ANISTR * myanpt;

	if (mapanimstrpt==NULL) return;
	myanpt = (ANISTR *) mapanimstrendpt; myanpt--;
	while (myanpt->antype!=-1)
	{
		if (myanpt->antype!=AN_NONE) { myanpt->ancount--; if (myanpt->ancount<0) {
		myanpt->ancount = myanpt->andelay;
		if (myanpt->antype==AN_LOOPF)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff++;
			if (myanpt->ancuroff==myanpt->anendoff) myanpt->ancuroff = myanpt->anstartoff;
		} }
		if (myanpt->antype==AN_LOOPR)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff--;
			if (myanpt->ancuroff==((myanpt->anstartoff)-1))
				myanpt->ancuroff = (myanpt->anendoff)-1;
		} }
		if (myanpt->antype==AN_ONCE)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff++;
			if (myanpt->ancuroff==myanpt->anendoff) { myanpt->antype = AN_ONCES;
				myanpt->ancuroff = myanpt->anstartoff; }
		} }
		if (myanpt->antype==AN_ONCEH)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) {
			if (myanpt->ancuroff!=((myanpt->anendoff)-1)) myanpt->ancuroff++;
		} }
		if (myanpt->antype==AN_PPFF)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff++;
			if (myanpt->ancuroff==myanpt->anendoff) { myanpt->ancuroff -= 2;
			myanpt->antype = AN_PPFR;
			if (myanpt->ancuroff<myanpt->anstartoff) myanpt->ancuroff++; }
		} } else {
		if (myanpt->antype==AN_PPFR)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff--;
			if (myanpt->ancuroff==((myanpt->anstartoff)-1)) { myanpt->ancuroff += 2;
			myanpt->antype = AN_PPFF;
			if (myanpt->ancuroff>myanpt->anendoff) myanpt->ancuroff --; }
		} } }
		if (myanpt->antype==AN_PPRR)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff--;
			if (myanpt->ancuroff==((myanpt->anstartoff)-1)) { myanpt->ancuroff += 2;
			myanpt->antype = AN_PPRF;
			if (myanpt->ancuroff>myanpt->anendoff) myanpt->ancuroff--; }
		} } else {
		if (myanpt->antype==AN_PPRF)
		{
			if (myanpt->anstartoff!=myanpt->anendoff) { myanpt->ancuroff++;
			if (myanpt->ancuroff==myanpt->anendoff) { myanpt->ancuroff -= 2;
			myanpt->antype = AN_PPRR;
			if (myanpt->ancuroff<myanpt->anstartoff) myanpt->ancuroff++; }
		} } }
	} } myanpt--; }
}


*/

/***
 * Release any allocate memory
 ***/
void MapFreeMem (void)
{
	int i;
	
	if (author!=NULL)		{free(author);author = NULL;}
	if (preferences!=NULL)	{free(preferences);preferences = NULL;}
	if (palette!=NULL) 		{free(palette); palette = NULL;}
	if (blocks!=NULL) 		{free(blocks); blocks = NULL;}
	
	if (animations!=NULL) 
	{
		ANMSTR *animation = (ANMSTR *)animations;
		for (i = 0; i < header->mapnumanimstr; i++)
		{
			if (animation->blocks != NULL)
			{
				free(animation->blocks);
				animation->blocks = NULL;
			}
			animation++;
		}
		free(animations);
		animations = NULL;
	}
	
	if (alternateGFX!=NULL) 	{free(alternateGFX); alternateGFX = NULL;}
	if (blockGFX!=NULL) 		{free(blockGFX); blockGFX = NULL;}
	
	//TODO : also in chunk, so double free !
	/*

	for (i = 0; i < 8; i++)
	{
		if (layers[i] != NULL)
		{
				free(layers[i]);
				layers[i] = NULL;
		}
	}
	*/
	
	memset (mapnovctext, 0, 70);

	//at last, free header (since header values could be used before)
	if (header!=NULL)		{free(header);header = NULL;}
	
	
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if (chunks[i].data != NULL)
		{
			chunks[i].name[0] = 0;
			chunks[i].size = 0;
			
			free(chunks[i].data);
			chunks[i].data = NULL;
		}
	}
}
/*
void MapRestore (void)
{
int i, j, k;
unsigned char * newgfxpt;

	if (mapgfxinbitmaps!=1 || abmTiles == NULL) return;
	i = 0; newgfxpt = mapblockgfxpt; while (abmTiles[i]!=NULL) {
		acquire_bitmap (abmTiles[i]);
		for (k=0;k<mapblockheight;k++) {
		for (j=0;j<mapblockwidth;j++) {
		switch (mapdepth) {
			case 8:
				putpixel (abmTiles[i], j, k, *((unsigned char *) newgfxpt));
				newgfxpt++;
				break;
			case 15:
			case 16:
				putpixel (abmTiles[i], j, k, *((unsigned short int *) newgfxpt));
				newgfxpt+=2;
				break;
			case 24:
				putpixel (abmTiles[i], j, k, makecol (newgfxpt[0], newgfxpt[1], newgfxpt[2]));
				newgfxpt+=3;
				break;
			case 32:
				putpixel (abmTiles[i], j, k, makecol (newgfxpt[1], newgfxpt[2], newgfxpt[3]));
				newgfxpt+=4;
				break;
		} } }
		release_bitmap (abmTiles[i]);
		i++;
	}
}

int MapRelocate2 (void)
{
int i, j, k;
BLKSTR * myblkstrpt;
//ANISTR * myanpt;
unsigned char * newgfxpt, * novcarray;
char ascnum[80];
//long int * myanblkpt;

	i = mapnumblockstr;
	myblkstrpt = (BLKSTR *) mapblockstrpt;

	novcarray = malloc (mapnumblockgfx);
	memset (novcarray, 0, mapnumblockgfx);
	i = 0; while (mapnovctext[i] != 0) {
		j = 0; while (mapnovctext[i] >= '0' && mapnovctext[i] <= '9') {
			ascnum[j] = mapnovctext[i];
			i++; j++;
		}
		ascnum[j] = 0;
		k = atoi(ascnum);
		if (k < 0 || k >= mapnumblockgfx) break;
		if (mapnovctext[i] == '-') {
			i++;
			j = 0; while (mapnovctext[i] >= '0' && mapnovctext[i] <= '9') {
				ascnum[j] = mapnovctext[i];
				i++; j++;
			}
			ascnum[j] = 0;
			j = atoi(ascnum);
			if (j < k || j >= mapnumblockgfx) break;
			while (k <= j) {
				novcarray[k] = 1; k++;
			}
		} else {
			novcarray[k] = 1;
		}
		if (mapnovctext[i] == ',') i++;
	}

	if (abmTiles == NULL) abmTiles = malloc ((sizeof (BITMAP *))*(mapnumblockgfx+2));
	abmTiles[0] = NULL;
	i = 0; newgfxpt = mapblockgfxpt; while (i<mapnumblockgfx) {
		abmTiles[i+1] = NULL;
		if (mapgfxinbitmaps==1 && novcarray[i]==0) {
			abmTiles[i] = create_video_bitmap (mapblockwidth, mapblockheight);
			if (abmTiles[i] != NULL) {
					mapblocksinvidmem++;
					acquire_bitmap (abmTiles[i]);
			} else {
				abmTiles[i] = create_bitmap (mapblockwidth, mapblockheight);
				if (abmTiles[i] == NULL) { MapFreeMem (); maperror = MER_CVBFAILED ; return -1; }
					mapblocksinsysmem++;
			}
			set_clip (abmTiles[i], 0, 0, 0, 0);
		} else {
			abmTiles[i] = create_bitmap (mapblockwidth, mapblockheight);
			if (abmTiles[i] == NULL) { MapFreeMem (); maperror = MER_CVBFAILED ; return -1; }
			mapblocksinsysmem++;
			set_clip (abmTiles[i], 0, 0, 0, 0);
		}
		for (k=0;k<mapblockheight;k++) {
		for (j=0;j<mapblockwidth;j++) {
		switch (mapdepth) {
			case 8:
				putpixel (abmTiles[i], j, k, *((unsigned char *) newgfxpt));
				newgfxpt++;
				break;
			case 15:
			case 16:
				putpixel (abmTiles[i], j, k, *((unsigned short int *) newgfxpt));
				newgfxpt+=2;
				break;
			case 24:
				putpixel (abmTiles[i], j, k, makecol (newgfxpt[0], newgfxpt[1], newgfxpt[2]));
				newgfxpt+=3;
				break;
			case 32:
				putpixel (abmTiles[i], j, k, makecol (newgfxpt[1], newgfxpt[2], newgfxpt[3]));
				newgfxpt+=4;
				break;
		} } }
		if (is_video_bitmap(abmTiles[i])) release_bitmap (abmTiles[i]);
		i++;
	}
	i = mapnumblockstr; while (i) {
		((BITMAP *) myblkstrpt->bgoff) = abmTiles[myblkstrpt->bgoff];
		if (myblkstrpt->fgoff!=0)
			((BITMAP *) myblkstrpt->fgoff) = abmTiles[myblkstrpt->fgoff];
		if (myblkstrpt->fgoff2!=0)
			((BITMAP *) myblkstrpt->fgoff2) = abmTiles[myblkstrpt->fgoff2];
		if (myblkstrpt->fgoff3!=0)
			((BITMAP *) myblkstrpt->fgoff3) = abmTiles[myblkstrpt->fgoff3];
		myblkstrpt++; i--;
	}

	free (novcarray);
	return 0;
}

int MapRelocate (void)
{
int i, j, cdepth, pixcol, ccr, ccg, ccb;
//BLKSTR * myblkstrpt;
unsigned char * oldgfxpt;
unsigned char * mycmappt;
unsigned char * newgfxpt;
unsigned char * newgfx2pt;

	if (screen == NULL) { MapFreeMem (); maperror = MER_NOSCREEN; return -1; }
	if (!gfx_capabilities&GFX_HW_VRAM_BLIT && mapgfxinbitmaps==1)
		{ MapFreeMem (); maperror = MER_NOACCELERATION; return -1; }
		cdepth = bitmap_color_depth (screen);
		oldgfxpt = (unsigned char *) mapblockgfxpt;
		newgfxpt = (unsigned char *)
			malloc (mapblockwidth*mapblockheight*((mapdepth+1)/8)*mapnumblockgfx*((cdepth+1)/8));
		if (newgfxpt==NULL) { MapFreeMem (); maperror = MER_OUTOFMEM; return -1; }
		newgfx2pt = newgfxpt;
		mycmappt = (unsigned char *) mapcmappt; pixcol = 0;
		for (i=0;i<(mapblockwidth*mapblockheight*mapnumblockgfx);i++)
		{
			switch (mapdepth) {
			case 8:
				if (cdepth==8) pixcol = (int) *oldgfxpt; else {
				j = (*oldgfxpt)*3;
				pixcol = makecol (mycmappt[j], mycmappt[j+1], mycmappt[j+2]);
#ifdef RB8BITTOPINK
				if (j == 0 && cdepth!=8) pixcol = makecol (255, 0, 255); }
#endif
				oldgfxpt++;
				break;
			case 15:
				ccr = ((((int) *oldgfxpt)&0x7C)<<1);
				ccg = ((((((int) *oldgfxpt)&0x3)<<3)|(((int) *(oldgfxpt+1))>>5))<<3);
				ccb = (((int) *(oldgfxpt+1)&0x1F)<<3);
				ccr |= ((ccr>>5)&0x07);
				ccg |= ((ccg>>5)&0x07);
				ccb |= ((ccb>>5)&0x07);
				pixcol = makecol (ccr, ccg, ccb);
				if (cdepth==8) { if (ccr == 0xFF && ccg == 0 && ccb == 0xFF) pixcol = 0; }
				oldgfxpt += 2;
				break;
			case 16:
				ccr = (((int) *oldgfxpt)&0xF8);
				ccg = ((((((int) *oldgfxpt)&0x7)<<3)|(((int) *(oldgfxpt+1))>>5))<<2);
				ccb = (((int) *(oldgfxpt+1)&0x1F)<<3);
				ccr |= ((ccr>>5)&0x07);
				ccg |= ((ccg>>6)&0x03);
				ccb |= ((ccb>>5)&0x07);
				pixcol = makecol (ccr, ccg, ccb);
				if (cdepth==8) { if (ccr == 0xFF && ccg == 0 && ccb == 0xFF) pixcol = 0; }
				oldgfxpt += 2;
				break;
			case 24:
				pixcol = makecol (*oldgfxpt, *(oldgfxpt+1), *(oldgfxpt+2));
				if (cdepth==8) { if (*oldgfxpt == 0xFF && *(oldgfxpt+1) == 0 &&
					*(oldgfxpt+2) == 0xFF) pixcol = 0; }
				oldgfxpt += 3;
				break;
			case 32:
				pixcol = makecol (*(oldgfxpt+1), *(oldgfxpt+2), *(oldgfxpt+3));
				if (cdepth==8) { if (*(oldgfxpt+1) == 0xFF && *(oldgfxpt+2) == 0 &&
					*(oldgfxpt+3) == 0xFF) pixcol = 0; }
				oldgfxpt += 4;
				break;
			}
			switch (cdepth) {
			case 8:
				*newgfxpt = (unsigned char) pixcol;
				newgfxpt++;
				break;
			case 15:
			case 16:
				*((unsigned short int *) newgfxpt) = (unsigned short int) pixcol;
				newgfxpt+=2;
				break;
			case 24:
				*newgfxpt = (unsigned char) (pixcol>>16)&0xFF;
				*(newgfxpt+1) = (unsigned char) (pixcol>>8)&0xFF;
				*(newgfxpt+2) = (unsigned char) pixcol&0xFF;
				newgfxpt+=3;
				break;
			case 32:
				*newgfxpt = 0;
				*(newgfxpt+1) = (unsigned char) (pixcol>>16)&0xFF;
				*(newgfxpt+2) = (unsigned char) (pixcol>>8)&0xFF;
				*(newgfxpt+3) = (unsigned char) pixcol&0xFF;
				newgfxpt+=4;
				break;
			}
		}
		free (mapblockgfxpt); mapblockgfxpt = (char *) newgfx2pt;

	mapdepth = cdepth;

	return MapRelocate2 ();
}
*/

/***
 * Header
 ***/
int MapDecodeMPHD (unsigned char * mdatpt, int chunkSize)
{
	//mdatpt += 8;
	
	debug_printf("enter MPHD\n");
	
	if (mdatpt[0] > 1) { maperror = MER_MAPTOONEW; return -1; }
	if (mdatpt[2] == 1) mapIsLSB = 1; else mapIsLSB = 0;

	header = (HDRSTR *) calloc(1, sizeof(HDRSTR)); //zeroed
	if (header==NULL) { maperror = MER_OUTOFMEM; return -1; }
	
	header->maptype = (int) mdatpt[3];
	if (header->maptype > FMP_10RLE) { maperror = MER_MAPTOONEW; return -1; }
	
	header->mapwidth = (short) getShort (mdatpt+4);
	header->mapheight = (short) getShort (mdatpt+6);
	//4 bytes unused
	header->mapblockwidth = (short) getShort (mdatpt+12);
	header->mapblockheight = (short) getShort (mdatpt+14);
	header->mapdepth = (short) getShort (mdatpt+16);
	header->mapblockstrsize = (short) getShort (mdatpt+18);
	header->mapnumblockstr = (short) getShort (mdatpt+20);
	header->mapnumblockgfx = (short) getShort (mdatpt+22);
	header->colourKey =  mdatpt[24];
	header->colourKeyHD.red = mdatpt[25];
	header->colourKeyHD.green = mdatpt[26];
	header->colourKeyHD.blue =  mdatpt[27];
	//we really don't care about this one...used nowhere since alt gfx are ALWAYS 8 bits (256color)
	//header->mapaltdepth = 8;

	//4 bytes unused
	if (chunkSize> 28) {
		header->mapblockgapx = (int) getShort (mdatpt+28);
		header->mapblockgapy = (int) getShort (mdatpt+30);
		header->mapblockstaggerx = (int) getShort (mdatpt+32);
		header->mapblockstaggery = (int) getShort (mdatpt+34);
	} else {
		header->mapblockgapx = (int) header->mapblockwidth;
		header->mapblockgapy = (int) header->mapblockheight;
		//header->mapblockstaggerx = 0;
		//header->mapblockstaggery = 0;
	}
	
	if (chunkSize > 36) 
		header->mapclickmask = (short) getShort (mdatpt+36);
	/*
	else 
		header->mapclickmask = 0;
	*/
	
	
	//2 bytes unused
	
	//header->mapnumanimstr = 0;
	//header->hasAlternateGFX = false;
	
	return 0;
}
unsigned char * MapEncodeMPHD( )
{
	unsigned char *data = calloc(1, 40);
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }

	data[0] = 1;
	data[2] = mapIsLSB;
	data[3] = (unsigned char) header->maptype;
	setShort(data+4, header->mapwidth);
	setShort(data+6, header->mapheight);
	//4 bytes unused 
	setShort(data+12, header->mapblockwidth);
	setShort(data+14, header->mapblockheight);
	setShort(data+16, header->mapdepth);
	setShort(data+18, header->mapblockstrsize);
	setShort(data+20, header->mapnumblockstr);
	setShort(data+22, header->mapnumblockgfx);
	data[24] = header->colourKey;
	data[25] = header->colourKeyHD.red;
	data[26] = header->colourKeyHD.green;
	data[27] = header->colourKeyHD.blue;
	setShort(data+28, header->mapblockgapx);
	setShort(data+30, header->mapblockgapy);
	setShort(data+32, header->mapblockstaggerx);
	setShort(data+34, header->mapblockstaggery);
	setShort(data+36, header->mapclickmask);
	//2 bytes unused
	
	return data;
}

/**
 * Editor prefs
 * 
 * For info only, since we don't really care !
 **/
int MapDecodeEDHD (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter EDHD\n");
	
	/*
	int i, j;
	short int * mybrshpt;
	char * mynamept;
	short int * mybrsh2pt;
	*/
	
	//calloc to get zero init (empty string)preferences
	preferences = (EDHDSTR *) calloc(1, sizeof(EDHDSTR));
	if (preferences==NULL) { maperror = MER_OUTOFMEM; return -1; }


	//mdatpt += 8;
	
	preferences->xmapoffset = (short) getShort (mdatpt);
	preferences->ymapoffset = (short) getShort (mdatpt+2);
	preferences->fgcolour = (int) getLong (mdatpt+4);
	preferences->bgcolour = (int) getLong (mdatpt+8);
	preferences->swidth = (short) getShort (mdatpt+12);
	preferences->sheight = (short) getShort (mdatpt+14);
	preferences->strtstr = (short) getShort (mdatpt+16);
	preferences->strtblk = (short) getShort (mdatpt+18);
	preferences->curstr = (short) getShort (mdatpt+20);
	preferences->curanim = (short) getShort (mdatpt+22); 
	//curanim = -1; //????
	
	preferences->animspd = (short) getShort (mdatpt+24);
	preferences->span = (short) getShort (mdatpt+26);
	preferences->numbrushes = (short) getShort (mdatpt+28);
	preferences->clickmask =(int) getShort (mdatpt+30);
	if ((preferences->clickmask<0) || (preferences->clickmask >= header->mapnumblockgfx)) preferences->clickmask = 0;

	//TODO ? we don't really care about brushes and there names...
	preferences->numbrushes = 0;
	/*
	for (i=0;i<preferences->numbrushes;i++)
	{
		j = *mybrshpt; 
		j *= *(mybrshpt+1); 
		j *= 2;
		j += 4;
		
		brshpt[i] = malloc (j); 
		j /= 2;
		mybrsh2pt = brshpt[i];
		*mybrsh2pt = *mybrshpt;
		mybrsh2pt++; mybrshpt++;
		*mybrsh2pt = *mybrshpt;
		mybrsh2pt++; mybrshpt++;
		j -= 2;
		if (header->maptype == 0) {
			while (j) {
				j--;
				*mybrsh2pt = *mybrshpt;
				if (*mybrsh2pt >= 0) *mybrsh2pt /= BLOCKSTRSIZE;
				mybrsh2pt++; mybrshpt++;
			}
		} else {
			while (j) {
				j--;
				*mybrsh2pt = *mybrshpt;
				if (*mybrsh2pt < 0) *mybrsh2pt *= sizeof(ANISTR);
				mybrsh2pt++; mybrshpt++;
			}
		}
	}
	
	mynamept = (char *) mybrshpt;
	if (chunkSize > (mynamept-((char *) mdatpt))) {
		for (i=0;i<8;i++) {
			if (brshpt[i] != NULL) {
				strcpy (brshname[i], mynamept);
				mynamept += strlen (mynamept);
				mynamept++;
			}
		}
	}
	*/

	return 0;
}
unsigned char *MapEncodeEDHD ( )
{
	unsigned char *data = calloc(1, 32);
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }
	
	setShort(data, preferences->xmapoffset);
	setShort(data+2, preferences->ymapoffset);
	setLong(data+4, preferences->fgcolour);
	setLong(data+8, preferences->bgcolour);
	setShort(data+12, preferences->swidth);
	setShort(data+14, preferences->sheight);
	setShort(data+16, preferences->strtstr);
	setShort(data+18, preferences->strtblk);
	setShort(data+20, preferences->curstr);
	setShort(data+22, preferences->curanim);
	setShort(data+24, preferences->animspd);
	setShort(data+26, preferences->span);
	setShort(data+28, 0); // preferences->numbrushes);
	setShort(data+30, preferences->clickmask);
	
	/*
	for (i=0;i<preferences->numbrushes;i++)
	{
		//TODO ?
	}
	*/

	return data;
}


/**
 * Option author  info.
 *
 * warning : strings are zero ended/separated
 **/
int MapDecodeATHR (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter ATHR\n");

	int i, j;
	//mdatpt += 8;
	
	//calloc to get zero init (empty string)
	author = (ATHSTR *) calloc(1, sizeof(ATHSTR));
	if (author==NULL) { maperror = MER_OUTOFMEM; return -1; }
	
	i = 0; j = 0;
	while (i < chunkSize)
	{
		author->name[j]=mdatpt[i];
		if (mdatpt[i]==0) break;
		i++;j++;
	}
	i++; j = 0;
	while (i < chunkSize)
	{
		author->info1[j]=mdatpt[i];
		if (mdatpt[i]==0) break;
		i++; j++;
	}
	i++; j = 0;
	while (i < chunkSize)
	{
		author->info2[j]=mdatpt[i];
		if (mdatpt[i]==0) break;
		i++; j++;
	}
	i++; j = 0;
	while (i < chunkSize)
	{
		author->info3[j]=mdatpt[i];
		if (mdatpt[i]==0) break;
		i++; j++;
	}
	return 0;
}
unsigned char *MapEncodeATHR( )
{
	int i, j;
	long size = 71*4;  //fixed size for easy save but not optimized 
	/*
	long size = strlen(author->name) + 1 + strlen(author->info1)+ 1 + strlen(author->info2)+ 1 + strlen(author->info3)+1;
	if (size%2 == 1)	size++;
	*/
	
	unsigned char *data = calloc(1, size);
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }
	
	//calloc to get zero init (empty string)
	author = (ATHSTR *) calloc(1, sizeof(ATHSTR));
	if (author==NULL) { maperror = MER_OUTOFMEM; return NULL; }
	
	//note : yes, i know, a for with a strlen is just a not-to-do but he! ;)
	
	i = 0; j = 0;
	for(i=0; i< strlen(author->name); i++,j++)
	{
		data[i+j] = author->name[i];
	}
	j++; //skip ending zero
	
	for(i=0; i< strlen(author->info1); i++,j++)
	{
		data[i+j] = author->info1[i];
	}
	j++; //skip ending zero
		
	for(i=0; i< strlen(author->info2); i++,j++)
	{
		data[i+j] = author->info2[i];
	}
	j++; //skip ending zero
	
	for(i=0; i< strlen(author->info3); i++,j++)
	{
		data[i+j] = author->info3[i];
	}
	j++; //skip ending zero
	
	return data;
}
/**
 * Color palette
 *
 * Should only be there for 8 bits FMAP but....
 **/
int MapDecodeCMAP (unsigned char * mdatpt, int chunkSize)
{
	//mdatpt += 8;
	
	sprintf(debug_string, "enter CMAP (%d bytes)\n",chunkSize);
	debug_printf(debug_string);
	
	palette =  malloc ( chunkSize );
	if (palette==NULL) { maperror = MER_OUTOFMEM; return -1; }
	memcpy (palette, mdatpt, chunkSize);
	
	return 0;
}
unsigned char *MapEncodeCMAP ( )
{
	int chunkSize = 256 * sizeof(COLORSTR);
	unsigned char *data = calloc(256, sizeof(COLORSTR));
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }

	memcpy (data, palette, chunkSize);
	
	return data;
}

/**
 * Blocks info (fixed tiles)
 * 
 * it doesn't include GFX : see BGFX
 **/
int MapDecodeBKDT (unsigned char * mdatpt, int chunkSize)
{
	int i; //, j;
	BLKSTR * block;
	long offset;
	
	debug_printf("enter BKDT\n");

	//mdatpt += 8;
	blocks = calloc (header->mapnumblockstr, sizeof(BLKSTR));
	if (blocks==NULL) { maperror = MER_OUTOFMEM; return -1; }

	for (i = 0; i< header->mapnumblockstr; i++)
	{
		offset = i*header->mapblockstrsize;
		block = &blocks[i];
		//offsets
		block->bgoff  = (int) getLong (mdatpt+offset);
		block->fgoff  = (int) getLong (mdatpt+offset+4);
		block->fgoff2 = (int) getLong (mdatpt+offset+8);
		block->fgoff3 = (int) getLong (mdatpt+offset+12);
		
		//FMP 0.5 used offset, not index...fix it to new FMP 1.0 format
		if (header->maptype == FMP_05) {
			block->bgoff /= (header->mapblockwidth*header->mapblockheight*((header->mapdepth+1)/8));
			block->fgoff /= (header->mapblockwidth*header->mapblockheight*((header->mapdepth+1)/8));
			block->fgoff2 /= (header->mapblockwidth*header->mapblockheight*((header->mapdepth+1)/8));
			block->fgoff3 /= (header->mapblockwidth*header->mapblockheight*((header->mapdepth+1)/8));
		}
		
		//long data
		block->user1 = (unsigned int) getLong (mdatpt+offset+16);
		block->user2 = (unsigned int) getLong (mdatpt+offset+20);
		
		//short data
		block->user3 = (unsigned short int) getShort (mdatpt+offset+24);
		block->user4 = (unsigned short int) getShort (mdatpt+offset+26);
		
		//byte data
		block->user5 = mdatpt[offset+28];
		block->user6 = mdatpt[offset+29];
		block->user7 = mdatpt[offset+30];
		
		//note : else unneeded because we used calloc (already zeroed)
		//event
		if (mdatpt[offset+31]&0x80) block->unused3 = 1; //else block->unused3 = 0;
		if (mdatpt[offset+31]&0x40) block->unused2 = 1; //else block->unused2 = 0;
		if (mdatpt[offset+31]&0x20) block->unused1 = 1; //else block->unused1 = 0;
		if (mdatpt[offset+31]&0x10) block->trigger = 1; //else block->trigger = 0;
		
		//collision
		if (mdatpt[offset+31]&0x08) block->br = 1; //else block->br = 0;
		if (mdatpt[offset+31]&0x04) block->bl = 1; //else block->bl = 0;
		if (mdatpt[offset+31]&0x02) block->tr = 1; //else block->tr = 0;
		if (mdatpt[offset+31]&0x01) block->tl = 1; //else block->tl = 0;

	}
	
	
	return 0;
}
unsigned char *MapEncodeBKDT(  )
{
	int i; //, j;
	long offset;
	unsigned char flags;
	BLKSTR * block;
	
	unsigned char *data = calloc (header->mapnumblockstr, header->mapblockstrsize);
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }

	for (i = 0; i< header->mapnumblockstr; i++)
	{
		offset = i*header->mapblockstrsize;
		block = &blocks[i];
		
		//offsets
		setLong(data + offset +  0, block->bgoff);
		setLong(data + offset +  4, block->fgoff);
		setLong(data + offset +  8, block->fgoff2);
		setLong(data + offset + 12, block->fgoff3);
		
		//long data
		setLong(data + offset + 16, block->user1);
		setLong(data + offset + 20, block->user2);
		
		//short data
		setShort(data + offset + 24, block->user3);
		setShort(data + offset + 26, block->user4);
		
		//byte data
		*(data + offset + 28) = block->user5;
		*(data + offset + 29) = block->user6;
		*(data + offset + 30) = block->user7;
		
		flags = 0;
		//event
		if (block->unused3) 	flags|=0x80;
		if (block->unused2) 	flags|=0x40;
		if (block->unused1) 	flags|=0x20;
		if (block->trigger) 	flags|=0x10;
		
		//collision
		if (block->br) 	flags|=0x8;
		if (block->bl) 	flags|=0x4;
		if (block->tr) 	flags|=0x2;
		if (block->tl) 	flags|=0x1;		
		
		*(data + offset + 31) = flags;
	}
	return data;
}

/**
 * Decode animation info (fixed tiles)
 **/
int MapDecodeANDT (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter ANDT\n");
	//ANDT
	//chunksize
	//array of long ended by FF
	//	EVERY tile used for ref or animation
	// ANISTR, one by animation
	// so to find animations
	// 1/ from end to FF / size(anistr)
	// 2/ from FF+1 to end, read every ANISTR
	// 3/ define anim.ref index
	// 4/ define anim.first index
	// 5/ play
	//===> format is though for anim playing, not anim storing :(
	

	int frameListSize, idx;
	int i,j;
	unsigned char *controls; //we can't use ANISTR since we need to handle LSB/MSB manually
	ANMSTR *animation;
	
	if (header == NULL)
	{
		debug_printf("Error : HEADER should be loaded first. Invalid file ?");
		maperror = MER_MAPLOADERROR;
		return -1; 
	}
	
	//mdatpt += 8;
	//chunkSize = getChunkSize(mdatpt-4);
	
	controls = mdatpt + chunkSize - sizeof(ANISTR);
	header->mapnumanimstr = 0;
	while (controls[0] != AN_END) {
		header->mapnumanimstr++;
		controls -= sizeof(ANISTR);
	}
	
	if (header->mapnumanimstr == 0)	return 0;
	
	animations  = calloc(header->mapnumanimstr, sizeof(ANMSTR)); //zeroed
	if (animations == NULL) { maperror = MER_OUTOFMEM; return -1; }
	

	//get back to first one
	animation = (ANMSTR *)animations;
	controls += sizeof(ANISTR);
	for (i= 0; i < header->mapnumanimstr; i++)
	{
		long first, last;
		
		animation->antype  = controls[0];
		animation->andelay = controls[1];
		animation->anuser  = controls[3];
		
		
		idx = getLong(controls+12);
		if (header->maptype == FMP_05)	idx /= 4;
		last = idx;
		
		idx = getLong(controls+8);
		if (header->maptype == FMP_05)	idx /= 4;
		first = idx;
		
		//get refblock, converted to short int
		idx = getLong( mdatpt + (first-1)*sizeof(long) );
		animation->refBlock = (short int) (idx & 0xFFFF);
		
		//get frames, converted to short int
		frameListSize = last - first;
		animation->frameCount = frameListSize;

		animation->blocks = calloc(frameListSize, sizeof(short int));
		if (animation->blocks == NULL) { maperror = MER_OUTOFMEM; return -1; }
		
		for (j = 0; j < frameListSize; j++)
		{
			idx = getLong( mdatpt + (first+j)*sizeof(long) );
			animation->blocks[j] = (short int) (idx & 0xFFFF);
		}

		controls += sizeof(ANISTR);
		animation++;
	}
	
	return 0;
}

/**
 * Decode raw alternate GFX
 *
 * Used for rendering only : tileset to use for a 24bit map on 8bit screen
 **/	
int MapDecodeAGFX (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter AGFX\n");
	
	//mdatpt+=8;
	alternateGFX = malloc (chunkSize);
	if (alternateGFX==NULL) { maperror = MER_OUTOFMEM; return -1; }
	memcpy (alternateGFX, mdatpt, chunkSize);
	
	header->hasAlternateGFX = true;
	return 0;
}
long mappy_getAlternateGFXSize()
{
	if (header == NULL)	return 0;
	
	return header->mapblockwidth*header->mapblockheight*header->mapnumblockgfx;
}

/**
 * RAW Block GFX, whatever depth is
 **/
int MapDecodeBGFX (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter BGFX\n");

	//mdatpt+=8;
	blockGFX = malloc (chunkSize);
	if (blockGFX==NULL) { maperror = MER_OUTOFMEM; return -1; }
	memcpy (blockGFX, mdatpt, chunkSize);
	
	return 0;
}
unsigned char *MapEncodeBGFX( )
{
	unsigned char *data = calloc (header->mapnumblockgfx, mappy_getBlockGFXSize());
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }

	memcpy(data, blockGFX, header->mapnumblockgfx*mappy_getBlockGFXSize());
	return data;
}

long mappy_getBlockGFXSize()
{
	if (header == NULL)	return 0;
	
	long base = header->mapblockwidth*header->mapblockheight;
	switch( header->mapdepth )
	{
		case 8:
			return base;
		case 15:
		case 16:
			return base*2;
		case 24:
			return base*3;
		case 32:
			return base*4;
	}
	
	return 0;
	
}

/**
 * Decode No VRAM Cache
 *
 * From Rob himself : "Used for organising video memory in some systems, largely not useful, can be ignored"*/
int MapDecodeNOVC (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter NOVC\n");
	
	//mdatpt+=8;
	memset (mapnovctext, 0, 70);
	if (chunkSize < 70) 
		strcpy (mapnovctext, (const char *)mdatpt);

	return 0;
}

/**
 * layer, 0-7 max
 * (O is BODY)
 *
 * Positive value = block idx
 * Negative value = anim idx
 **/
int MapDecodeLayer (unsigned char * mdatpt, int chunkSize, int lnum)
{
	debug_printf("enter LAYx\n");
	int i, j, k;
	signed short int index;
	signed short int *layer;
	
	//mdatpt += 8;
	layers[lnum] = (signed short int*) calloc(header->mapwidth*header->mapheight, sizeof(signed short int));
	if (layers[lnum] == NULL) { maperror = MER_OUTOFMEM; return -1; }

	layer = layers[lnum];
	for (j=0;j<header->mapheight;j++)
	{
		for (i=0;i<header->mapwidth;i++)
		{
			index = (signed short int) getShort (mdatpt);
			if (index < 0)
			{
				//anim
				switch(header->maptype)
				{
					case FMP_05: 
						*layer = index/16;
						break;
					case FMP_10:
						*layer = index;
						break;
					case FMP_10RLE:
						k = index;
						index = (short int) getShort (mdatpt);
						while (k) {
							*layer = index;
							i++;
							if (++k)
							{	
								layer++;
							}
						}
						break;					
				}
			}
			else if (index > 0) //we get rid of k = 0, invalid for RLE
			{
				//block
				switch(header->maptype)
				{
					case FMP_05: 
						*layer = index / header->mapblockstrsize;	//offset to index
						break;
					case FMP_10:
						*layer = index;
						break;
					case FMP_10RLE:
						k = index;
						//FAKE RLE on block
						while (k) {
							index = (short int) getShort (mdatpt);
							*layer = index;
							i++;
							if (--k)
							{	
								layer++;
								mdatpt += 2;
							}
						}
						break;
				}
			}
			
			layer++;
			mdatpt+=2;
		}
	}
	

	return 0;
}

unsigned char *MapEncoreLayer(int lnum)
{
	unsigned char *data = calloc(header->mapwidth*header->mapheight, sizeof(signed short int));
	if (data==NULL) { maperror = MER_OUTOFMEM; return NULL; }

	memcpy(data, layers[lnum], header->mapwidth*header->mapheight*sizeof(signed short int));
	return data;
}


/**
 * Decode new layer format
 *
 * To extend the 8 layers limit and add layer name
 **/
int MapDecodeLayerNew (unsigned char * mdatpt, int chunkSize)
{
	debug_printf("enter NLAY\n");
	
	//TODO
	
	debug_printf("This new layer format is not known");
	return 0;
	
}
	
/**
 * Decode an user defined chunk
 *
 * FMP support any chunk which follow the ID + SIZE + DATA format
 **/
static int MapDecodeUnknown (char *name, int chunkSize, unsigned char *data)
{
	//TODO keep it for reuse on export
	sprintf(debug_string, "Unknown chunk found : %s\n", name);
	debug_printf(debug_string);
	return 0;
}



/*****
 * Decode every map chunk
 *
 ****/
static int MapRealDecode (unsigned char * mmpt, long int mpfilesize)
{
	int chkdn;

	MapFreeMem ();
	
	mpfilesize -= 12;
	mapIsLSB = false;

	while (mpfilesize > 0) {
		char chunkName[5];
		int chunkSize = getChunkSize(mmpt+4);
		unsigned char *chunkData;
		
		strncpy(chunkName, (const char *)mmpt, 4);
		chunkData = malloc(chunkSize);
		if (chunkData == NULL)	{ maperror = MER_OUTOFMEM; return -1; }
		memcpy(chunkData, mmpt+4+4, chunkSize);
		
		addChunk(chunkName, chunkSize, chunkData);

		mmpt += 4 + 4 + chunkSize;//header + chunkSize + data
		mpfilesize -= 4 + 4 + chunkSize;
		

		chkdn = 0;

		if (!strncmp ((const char *)chunkName, "ATHR", 4)) { chkdn = 1; MapDecodeATHR (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "MPHD", 4)) { chkdn = 1; MapDecodeMPHD (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "EDHD", 4)) { chkdn = 1; MapDecodeEDHD (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "CMAP", 4)) { chkdn = 1; MapDecodeCMAP (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "BKDT", 4)) { chkdn = 1; MapDecodeBKDT (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "ANDT", 4)) { chkdn = 1; MapDecodeANDT (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "AGFX", 4)) { chkdn = 1; MapDecodeAGFX (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "BGFX", 4)) { chkdn = 1; MapDecodeBGFX (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "NOVC", 4)) { chkdn = 1; MapDecodeNOVC (chunkData, chunkSize); }
		if (!strncmp ((const char *)chunkName, "BODY", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 0); }
		if (!strncmp ((const char *)chunkName, "LYR1", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 1); }
		if (!strncmp ((const char *)chunkName, "LYR2", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 2); }
		if (!strncmp ((const char *)chunkName, "LYR3", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 3); }
		if (!strncmp ((const char *)chunkName, "LYR4", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 4); }
		if (!strncmp ((const char *)chunkName, "LYR5", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 5); }
		if (!strncmp ((const char *)chunkName, "LYR6", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 6); }
		if (!strncmp ((const char *)chunkName, "LYR7", 4)) { chkdn = 1; MapDecodeLayer(chunkData, chunkSize, 7); }
		if (!strncmp ((const char *)chunkName, "NLYR", 4)) { chkdn = 1; MapDecodeLayerNew (chunkData, chunkSize); }
		
		if (!chkdn) MapDecodeUnknown (chunkName, chunkSize, chunkData);


		if (maperror != MER_NONE) { MapFreeMem (); return -1; }
	}

	//convert to bitmap only when asked
	return 0; //MapRelocate ();
}



int MapDecode (unsigned char * mapmempt)
{
	long maplength;

	MapFreeMem ();
	maperror = 0;

	if (*mapmempt!='F') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+1)!='O') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+2)!='R') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+3)!='M') maperror = MER_MAPLOADERROR;
	mapmempt += 4;
	maplength = (getChunkSize (mapmempt))+8;

	if (maperror) return -1;
	mapmempt += 4;

	if (*mapmempt!='F') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+1)!='M') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+2)!='A') maperror = MER_MAPLOADERROR;
	if (*(mapmempt+3)!='P') maperror = MER_MAPLOADERROR;
	mapmempt+=4;

	if (maperror) return -1;
	return MapRealDecode (mapmempt, maplength);
}

////////////////////////////////////////////////////////////////////////
///// 
///// PUBLIC 
/////
////////////////////////////////////////////////////////////////////////

CHUNK *mappy_getChunk(char *name)
{
	int i;
	if (strlen(name) != 4)	
	{
		debug_printf("Wanted chunk's name must be 4 chars long\n");
		return NULL;
	}
	
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if (chunks[i].size != 0)
		{
			if ( strncmp(chunks[i].name, name, 4) == 0 )	return &chunks[i];
		}
	}
	
	sprintf(debug_string, "Chunk %s not found\n", name);
	debug_printf(debug_string);
	return NULL;
}
int mappy_setChunk(char *name, int size, unsigned char *data)
{
	int i;
	
	if (strlen(name) != 4)	
	{
		debug_printf("Chunk name must be 4 chars long\n");
		return -1;
	}
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if ( strncmp(chunks[i].name, name, 4) == 0 )
		{
			//free prev data
			if (chunks[i].data)	free(chunks[i].data);
			
			//set new one, size updated
			chunks[i].size = size;
			chunks[i].data = data;
			return 0;
		}
	}
	
	//not chunk with this name found, so create one
	return addChunk(name, size, data);
}


HDRSTR *mappy_getHeader( )
{
	return header;
}
void mappy_setHeader(HDRSTR *newHeader)
{
	//use it at your own risk : if value in header are diff from value expected on others chunks, you're dead !
	if (header!=NULL)		{free(header);header = NULL;}
	
	header = newHeader;
	
	//only work with these values so force them there
	header->mapblockstrsize = mappy_getBlockGFXSize(); //8x8 *1byte
	
	header->colourKey =  0;
	header->colourKeyHD.red = 0xFF;
	header->colourKeyHD.green = 0;
	header->colourKeyHD.blue  = 0xFF;
	
	header->maptype = 1; // FMP 1.0
	header->mapclickmask = 0;								//block ID used as mask for pixel perfect click
	header->mapblockgapx = header->mapblockwidth;
	header->mapblockgapy = header->mapblockheight; 			//gap between 2 blocks (useful for iso where x = blockwidth/2 and y = blockheight/2)
	header->mapblockstaggerx = header->mapblockstaggery = 0; 	//isometric offsets

	

	
	mappy_setChunk("MPHD", 40, MapEncodeMPHD());
}


ATHSTR *mappy_getAuthorInfo()
{
	return author;
}
void mappy_setAuthorInfo(ATHSTR *newAuthor)
{
	if (author != NULL)	free(author);
	author = newAuthor;
	mappy_setChunk("MPHD", 71*4, MapEncodeATHR());
}

COLORSTR *mappy_getColors()
{
	return palette;
}
void mappy_setColors( COLORSTR *newColors)
{
	if (palette!=NULL) 		{free(palette); palette = NULL;}
	palette = newColors;
	mappy_setChunk("CMAP", 256*sizeof(COLORSTR), MapEncodeCMAP());
}


BLKSTR *mappy_getBlocks()
{
	return blocks;
}
void mappy_setBlocks(BLKSTR *newBlocks)
{
	if (blocks != NULL)	free(blocks);
	blocks = newBlocks;
	mappy_setChunk("BKDT", header->mapnumblockstr*header->mapblockstrsize, MapEncodeBKDT());
}

ANMSTR (*mappy_getAnimations())[]
{
	return animations;
}
unsigned char *mappy_getAlternateGFX()
{
	return alternateGFX;
}
unsigned char *mappy_getBlockGFX()
{
	return blockGFX;
}
void mappy_setBlockGFX(unsigned char *newGFX)
{
	char pixelSize = 1;	//8bits
	if (header->mapdepth>8) pixelSize++; //15bits/16bits
	if (header->mapdepth>16) pixelSize++;//24bits
	if (header->mapdepth>24) pixelSize++;//32bits
	
	if (blockGFX != NULL)	free(blockGFX);
	blockGFX = newGFX;

	mappy_setChunk("BGFX", header->mapnumblockgfx*header->mapblockwidth*header->mapblockheight*pixelSize, MapEncodeBGFX());
}

signed short int *mappy_getLayer(unsigned char id)
{
	if (id < 8)
		return layers[id];
	
	return NULL; //TODO (see NLYR)
}
void mappy_setLayer(unsigned char id, signed short int *newMap)
{
	if (id > 8)	return;
	
	if (layers[id] != NULL)	free(layers[id]);
	layers[id] = newMap;

	if (id == 0)
	{
		mappy_setChunk("BODY", header->mapwidth*header->mapheight*sizeof(short int), MapEncoreLayer(id));
	}
	else
	{
		char chunkName[5];
		sprintf(chunkName, "LAY%1d", id);
		mappy_setChunk(chunkName, header->mapwidth*header->mapheight*sizeof(short int), MapEncoreLayer(id));
	}
}


/***
 * Create a blank FMP, ready to be filled by MapEncodeXXX() or mappy_setChunk()
 ***/
void mappy_new( )
{
	mappy_close();
	maperror = MER_NONE;
	
	//TODO use param
	mapIsLSB = 1; 
	
	//add defaults
	// header (100x100, 8x8, 8bits, 1 block)
	HDRSTR *defaultHeader = calloc(1, sizeof(HDRSTR)); //zeroed
	defaultHeader->mapwidth = 100;
	defaultHeader->mapheight = 100;
	defaultHeader->mapblockwidth=8;
	defaultHeader->mapblockheight=8;
	defaultHeader->mapdepth = 8;
	defaultHeader->mapnumblockstr = 1;
	defaultHeader->mapnumblockgfx = 1;	
	defaultHeader->mapnumanimstr = 0;
	defaultHeader->hasAlternateGFX=0;
	mappy_setHeader(defaultHeader);	 //add missing values and encode it
	
	//zeroed preferences
	preferences = (EDHDSTR *) calloc(1, sizeof(EDHDSTR));
	mappy_setChunk("EDHD", sizeof(EDHDSTR), MapEncodeEDHD()); //encode it first
	
	COLORSTR *emptyColor = calloc(256, sizeof(COLORSTR));
	mappy_setColors(emptyColor); //encode it first
	
	BLKSTR *emptyBlock = calloc(defaultHeader->mapnumblockgfx, sizeof(BLKSTR));
	mappy_setBlocks(emptyBlock); //encode it first
	
	unsigned char *emptyGFX = calloc(1, mappy_getBlockGFXSize());
	mappy_setBlockGFX(emptyGFX);  //free previous
	
	signed short int *emptyLayer = calloc(defaultHeader->mapwidth*defaultHeader->mapheight, sizeof(short int));
	mappy_setLayer(0, emptyLayer);  //free previous
}



/***
*	Load FMP file 
****/
int mappy_load(char *mapname)
{
	int mretval;
	unsigned char *fileData;
	int fileSize;

	mappy_close();
	maperror = MER_NONE;

	mapfilept = fopen (mapname, "rb");
	if (mapfilept==NULL) { maperror = MER_NOOPEN; return -1; }
	
	fileSize = fsize(mapfilept);
	if (fileSize == 0)	{ fclose(mapfilept); maperror = MER_MAPLOADERROR; return -1;}
	
	fileData = malloc(fileSize);
	if (fileData == NULL) { fclose(mapfilept);maperror = MER_OUTOFMEM; return -1; }
	
	fread(fileData, fileSize, 1, mapfilept);
	fclose(mapfilept);
	
	mretval = MapDecode(fileData);
	
	return mretval;
}


/***
*	Save FMP file 
****/
int mappy_save(char *mapname)
{
	int i;
	long fileSize = 0;
	unsigned char *filedata, *mapData;

	maperror = MER_NONE;
	
	fileSize+= 4; // FORM 
	fileSize+= 4; // filesize value
	
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if (chunks[i].size)
		{
			fileSize += 4;	//chunk ID
			fileSize += 4;	//chunk size
			fileSize += chunks[i].size;
			//printf("\nchunk %s is %d bytes =>%ld", chunks[i].name, chunks[i].size, fileSize);
		}
	}
	fileSize+= 4; //FMAP
	 
	filedata = calloc(1, fileSize); 
	if (filedata==NULL) { maperror = MER_OUTOFMEM; return -1; }
	mapData = filedata,
	
	*(mapData+0) = 'F';
	*(mapData+1) = 'O';
	*(mapData+2) = 'R';
	*(mapData+3) = 'M';
	mapData+=4;
	
	setChunkSize(mapData, fileSize-8);
	mapData+=4;

	*(mapData+0) = 'F';
	*(mapData+1) = 'M';
	*(mapData+2) = 'A';
	*(mapData+3) = 'P';
	mapData+=4;
	
	for (i = 0; i < MAX_CHUNK; i++)
	{
		if (chunks[i].size != 0)
		{
			memcpy(mapData, chunks[i].name, 4);
			mapData+=4;
			
			setChunkSize(mapData, chunks[i].size);
			mapData+=4;
			
			memcpy(mapData, chunks[i].data, chunks[i].size);
			mapData+=chunks[i].size;
		}
	}
	
	mapfilept = fopen (mapname, "wb");
	if (mapfilept==NULL) { free(filedata); maperror = MER_NOOPEN; return -1; }
	
	fwrite(filedata, 1, fileSize, mapfilept);
	fclose(mapfilept);
	free(filedata);
	
	return 0;
}

/**
 * Close previously open map, if any
 **/
void mappy_close(void)
{
	MapFreeMem();
}

int mappy_getLastErrorID()
{
	return maperror;
}
