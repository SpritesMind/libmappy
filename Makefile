include makerules 

SRCS = $(wildcard $(SRCDIR)$(SEP)*.c)
OBJS = $(patsubst %.c,%.o,$(SRCS))

#OBJ_DEBUG = $(addprefix $(OBJDIR_DEBUG)$(SEP), $(OBJS))
#OBJ_RELEASE = $(addprefix $(OBJDIR_RELEASE)$(SEP), $(OBJS))
OBJ_DEBUG = $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR_DEBUG)$(SEP)%,$(OBJS))
OBJ_RELEASE =  $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR_RELEASE)$(SEP)%,$(OBJS))

all:  debug release

ee:
	$(ECHO) $(SRCS)
	$(ECHO) $(OBJS)


debug: $(OUT_DEBUG)$(SEP)libmappyd.a

$(OUT_DEBUG)$(SEP)libmappyd.a: folders $(OBJ_DEBUG) $(DEP_DEBUG) 
	$(AR) $@ $(OBJ_DEBUG)

$(OBJDIR_DEBUG)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS_DEBUG) $(INC_DEBUG) -c -o $@ $<

	
release: $(OUT_RELEASE)$(SEP)libmappy.a

$(OUT_RELEASE)$(SEP)libmappy.a: folders $(OBJ_RELEASE) $(DEP_RELEASE)
	$(AR) $@ $(OBJ_RELEASE)

$(OBJDIR_RELEASE)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS_RELEASE) $(INC_RELEASE) -c -o $@ $<



clean:  clean_debug  clean_release
	-$(RMD) $(OBJDIR) $(HIDE_ERROR)
	
clean_debug: 
	@echo Clean debug files and folders
	-$(RM) $(OBJ_DEBUG) $(HIDE_ERROR)
	-$(RM) $(OUT_DEBUG)$(SEP)libmappyd.a $(HIDE_ERROR)

clean_release: 
	@echo Clean release files and folders
	-$(RM) $(OBJ_RELEASE) $(HIDE_ERROR)
	-$(RM) $(OUT_RELEASE)$(SEP)libmappy.a  $(HIDE_ERROR)

folders: bin $(OBJDIR) $(OBJDIR_DEBUG) $(OBJDIR_RELEASE) $(OUT_DEBUG) $(OUT_RELEASE)

bin:
	$(MKDIR) $@
	
$(OBJDIR):
	$(MKDIR) $@
	
$(OBJDIR_DEBUG):
	$(MKDIR) $@

$(OUT_DEBUG):
	$(MKDIR) $@

$(OBJDIR_RELEASE):
	$(MKDIR) $@

$(OUT_RELEASE):
	$(MKDIR) $@

	
.PHONY: all debug release clean clean_debug clean_release folders
